package com.example.karol.cuartoa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadPrincipal extends AppCompatActivity {
    Button botonLoging, botonIngresar , botonBuscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);

        botonLoging=(Button)findViewById(R.id.Loging);
        botonIngresar= (Button) findViewById(R.id.Buscar);
        botonBuscar=(Button)findViewById(R.id.Guaradar);


        botonLoging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ActividadPrincipal.this,Buscar.class );

                startActivity(intent);

            }


        });

        botonIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActividadPrincipal.this,guardar.class) ;
                startActivity(intent);
            }
        });



        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ActividadPrincipal.this,cuartoa.class);
                startActivity(intent);

            }
        });



    }
}
